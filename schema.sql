-- database "db" and schema "public" will exist by default

\connect db;


create table public.alert_subscription (
    id serial primary key,
    email_recipient text not null check (email_recipient ~* '^.+@.+\..+$'),
    product_key text not null,
    product_title text not null,
    product_page_url text not null,
    product_image_url text not null,
    product_image_url_2 text not null,
    created_at timestamp default now()
);



create table public.admin (
    id serial primary key,
    email text not null unique check (email ~* '^.+@.+\..+$')
);

create schema private;

create table private.admin (
    admin_id integer primary key references public.admin(id) on delete cascade,
    password_hash text not null
);


